def ruler(n):
    i = 1
    j = 1
    for _ in range(n):
        if i == 10:
            i = 0
            print(j, end="")
            j += 1
            i += 1
        else:
            i += 1
            print(" ", end="")
    print()
    i = 1
    for _ in range(n):
        if i == 10:
            i = 0
            print(i, end="")
            i += 1
        else:
            print(i, end="")
            i += 1
ruler(80)
print()
ruler(5)
print()
ruler(10)
print()
ruler(25)
print()
ruler(51)
print()
ruler(80)
