def isListOfInts(list1):
    a = 0
    for i in list1:
        if isinstance(i,int):
            pass
        else:
            a = 1
            break
    if a == 1:
        return False
    else:
        return True

def testList(list1):
    if isinstance(list1, list):
        print(list1 ,'-->',isListOfInts(list1))
    else:
        print('ValueError: ',list1,'- arg not of <list> type')


testList([])
testList([1])
testList([1,2])
testList([0])
testList(['1'])
testList([1,'a'])
testList(['a',1])
testList([1,1.0])
testList([1.0,1.0])
testList((1,2))